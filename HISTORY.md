# Change log

## [0.1.0] - 2018-11-18

### Added
- `get` function
- `build_url` function

### Updated
- `post` function returns only post id

## [0.0.1] - 2018-11-13

### Added
- `post` function
